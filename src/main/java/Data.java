import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Data implements Serializable {
    private long id;
    private String name;
    private LocalDateTime date;

    public Data(long id, String name, LocalDateTime date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return id == data.id &&
                Objects.equals(name, data.name) &&
                Objects.equals(date, data.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, date);
    }
}