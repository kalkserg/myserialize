import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        long ID = 123456789;
        String name = new String("Tom");
        LocalDateTime date = LocalDateTime.now();

        Data dataObjBefore = new Data(ID, name, date);
        Data dataObjAfter;

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data.out"));
        out.writeObject(dataObjBefore);
        out.close();

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data.out"));
        dataObjAfter = (Data) in.readObject();

        System.out.println("Before: \n" + dataObjBefore);
        System.out.println("After: \n" + dataObjAfter);
    }
}